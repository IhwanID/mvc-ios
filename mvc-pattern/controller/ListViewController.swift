//
//  ViewController.swift
//  mvc-pattern
//
//  Created by Ihwan ID on 28/08/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController {

    var courses = [Course]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        getData()
    }

    func setupTableView(){
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        tableView.tableFooterView = UIView()

    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cellID")
        cell.textLabel?.text = "\(courses[indexPath.row].name)"
        print("\(courses[indexPath.row].name)")
        return cell
    }

    func getData(){

        Service.shared.fetchCourse { (courses, error) in
            if let error = error{
                print(error)
                return
            }

            if let courses = courses{
                print(courses.count)
                self.courses.append(contentsOf: courses)
                print(self.courses.count)
                self.tableView.reloadData()
            }

        }

    }
}

