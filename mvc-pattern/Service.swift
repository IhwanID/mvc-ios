//
//  Service.swift
//  mvc-pattern
//
//  Created by Ihwan ID on 28/08/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import Foundation

class Service: NSObject{
    static let shared = Service()

    func fetchCourse(completion: @escaping ([Course]?, Error?) -> ()){
        let urlString = "https://api.letsbuildthatapp.com/jsondecodable/courses"

        guard let url = URL(string: urlString) else {
            return
        }
        URLSession.shared.dataTask(with: url){(data, response, error) in
            if let err = error{
                completion(nil, err)
                print(err)
                print("failed to fetch! \(error)")
                return
            }

            guard let data = data else {return}

            do {
                let courses = try JSONDecoder().decode([Course].self, from: data)
                DispatchQueue.main.async {
                    completion(courses,nil)
                }
            } catch {
                print("Error: \(error.localizedDescription)")
                return
            }
        }.resume()

    }
}
