//
//  Course.swift
//  mvc-pattern
//
//  Created by Ihwan ID on 28/08/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import Foundation

struct Course: Codable{
    let id: Int
    let name: String
    let imageUrl: String
    let link: String
    let number_of_lessons: Int
}
